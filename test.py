import unittest
import random
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from url import URL

options = webdriver.ChromeOptions()
options.add_argument('--headless')
options.add_argument('--disable-gpu')
browser = webdriver.Chrome('chromedriver.exe')
try:
    

    browser.get(URL)
            
    browser.implicitly_wait(60)
    sign_in = browser.find_element_by_name('emailOrPhone')
    sign_in.clear()
    sign_in.send_keys('admin@example.com')
    browser.implicitly_wait(60)
    password = browser.find_element_by_name('password')
    password.clear()
    password.send_keys('password')
    browser.implicitly_wait(60)
    password.submit()
    browser.implicitly_wait(60)
    browser.implicitly_wait(60)
    reg = browser.find_element_by_xpath("//span[contains(text(),'Registration')]")
    reg.click()
    sample_names = ['John', 'Davis', 'Dayo', 'Funke', 'Bisi', 'Amina', 'Obi', 'Ahmed']
    sample_surnames = ['Adebayo', 'Ndidi', 'Musa', 'Okeke', 'Okafor', 'Mohammed', 'Oluwalola', 'Etibong']
    browser.implicitly_wait(60)
    surname_field = browser.find_element_by_xpath("//input[@placeholder='e.g. Ciroma']")
    first_name_field = browser.find_element_by_xpath("//input[@placeholder='e.g. Chukwuma']")
    sex_male = browser.find_element_by_xpath("//span[contains(text(),'Male')]")
    sex_female = browser.find_element_by_xpath("//span[contains(text(),'Female')]")
    age_day = browser.find_element_by_xpath("//div[@placeholder='DD']//span[@class='btn btn-default form-control ui-select-toggle']")
    age_day.click()
    age_day = browser.find_element_by_xpath('//*[@id="ui-select-choices-row-10-%d"]/span'% random.randint(0,29))
    age_day.click()
    age_month = browser.find_element_by_xpath("//div[@placeholder='MM']//span[@class='btn btn-default form-control ui-select-toggle']")
    age_month.click()
    age_month = browser.find_element_by_xpath('//*[@id="ui-select-choices-row-11-%d"]/span' % random.randint(0,12))
    age_month.click()
    age_year = browser.find_element_by_xpath("//div[@placeholder='YYYY']//span[@class='btn btn-default form-control ui-select-toggle']")
    age_year.click()
    age_year = browser.find_element_by_xpath('//*[@id="ui-select-choices-row-12-%d"]/span' % random.randint(0,101))
    age_year.click()
    phone = browser.find_element_by_xpath("//tel-input[@name='phone']//input[@placeholder='0802 123 4567']")
    register_btn = browser.find_element_by_xpath("//button[contains(text(),'Register Only')]")
    surname = random.choice(sample_surnames) 
    first_name = random.choice(sample_names)
    surname_field.send_keys(surname)
    first_name_field.send_keys(first_name)
    if random.randint(0,101) % 2 == 0:
        sex_female
    else:
        sex_male.click()
    phone.send_keys('09086788765')
    register_btn.click()
    browser.implicitly_wait(60)
    patient_name = browser.find_element_by_xpath("//*[text()='"+ first_name + "  "  + surname + "']")
    print(patient_name.text)
 
    #patient_name.click()
    print('Registered ' + ' - ' + first_name + ' ' + surname )
    lines = ['first_name, surname', first_name + '  ' + surname]
    try:
        with open("test_patient.txt", "w") as file:
            file.write('\n'.join(lines))
        print('Wrote Patient Name To "test_patient.txt"')
    except Exception as e:
        print(e)
    patient_name.click()
    assert patient_name.text == first_name + '  ' + surname
    browser.quit()



except Exception as e:
    print(e)
    browser.quit()
