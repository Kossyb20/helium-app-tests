import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from url import URL




class FormsTestCase(unittest.TestCase):

    def setUp(self):
        #self.browser = webdriver.Chrome('chromedriver.exe')
        options = webdriver.ChromeOptions()
        options.add_argument('--headless')
        self.browser = webdriver.Chrome('chromedriver.exe', chrome_options=options)
  

    def test_login(self):
        url = URL
        browser = self.browser
        browser.get(url)
        browser.implicitly_wait(60)
        sign_in = browser.find_element_by_name('emailOrPhone')
        sign_in.clear()
        sign_in.send_keys('admin@example.com')
        browser.implicitly_wait(60)
        password = browser.find_element_by_name('password')
        password.clear()
        password.send_keys('password')
        browser.implicitly_wait(60)
        password.submit()
     



    def test_forms_settings(self):
        self.test_login()
        self.browser.implicitly_wait(60)
        settings = self.browser.find_element_by_xpath('//*[@id="main-container"]/ui-view/panel/nav-sidebar/div/div/div[1]/div[2]/div[3]/a')
        settings.click()
        forms = self.browser.find_element_by_xpath('//*[@id="main-container"]/ui-view/panel/nav-sidebar/div/div/div[1]/div[2]/div[3]/div/a[3]')
        forms.click()
        print('Forms')

    
    def tearDown(self):
        self.browser.quit()
