# tests/runner.py
import unittest
from io import StringIO
import HtmlTestRunner

# import your test modules
import appoints_test
import features_test
import finance_test
import forms_test
import insights_test
import lab_test
import patient_test
import pharmacy_test
import profile_test
import reg_test
import staff_test

# initialize the test suite
loader = unittest.TestLoader()
suite  = unittest.TestSuite()

# add tests to the test suite

suite.addTests(loader.loadTestsFromModule(finance_test))
suite.addTests(loader.loadTestsFromModule(features_test))
suite.addTests(loader.loadTestsFromModule(appoints_test))
suite.addTests(loader.loadTestsFromModule(forms_test))
suite.addTests(loader.loadTestsFromModule(insights_test))
suite.addTests(loader.loadTestsFromModule(lab_test))
suite.addTests(loader.loadTestsFromModule(patient_test))
suite.addTests(loader.loadTestsFromModule(pharmacy_test))
suite.addTests(loader.loadTestsFromModule(profile_test))
suite.addTests(loader.loadTestsFromModule(reg_test))
suite.addTests(loader.loadTestsFromModule(staff_test))

# initialize a runner, pass it your suite and run it
#runner = unittest.TextTestRunner(verbosity=3)
#html_file = open('test_report.html', 'wb')

runner = HtmlTestRunner.HTMLTestRunner(output='/Users/Kossy/documents/helium/product_test/codebase')
result = runner.run(suite)
