import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from url import URL






class PatientTestCase(unittest.TestCase):

    def setUp(self):
        #self.browser = webdriver.Chrome('chromedriver.exe')
        options = webdriver.ChromeOptions()
        options.add_argument('--headless')
        self.browser = webdriver.Chrome('chromedriver.exe', chrome_options=options)
  

    def test_login(self):
        url = URL
        browser = self.browser
        browser.get(url)
        browser.implicitly_wait(60)
        sign_in = browser.find_element_by_name('emailOrPhone')
        sign_in.clear()
        sign_in.send_keys('admin@example.com')
        browser.implicitly_wait(60)
        password = browser.find_element_by_name('password')
        password.clear()
        password.send_keys('password')
        browser.implicitly_wait(60)
        password.submit()
     

    def test_all_patients(self):
        
        self.test_login()
        self.browser.implicitly_wait(60)
        patient_title = self.browser.find_element_by_class_name('Patients__title')
        span = self.browser.find_element_by_class_name('Patients__patient-name')
        span.click()
        print('All Patients')



    def tearDown(self):
        self.browser.quit()





# if __name__ == '__main__':
#     HTMLTestRunner.main(verbosity=2)
