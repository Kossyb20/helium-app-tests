import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from url import URL




class CheckinTestCase(unittest.TestCase):

    first_name = ''
    surname = ''

    def setUp(self):
        self.browser = webdriver.Chrome('chromedriver.exe')
        # options = webdriver.ChromeOptions()
        # options.add_argument('--headless')
        # self.browser = webdriver.Chrome('chromedriver.exe', chrome_options=options)
  

    def test_login(self):
        url = URL
        browser = self.browser
        browser.get(url)
        browser.implicitly_wait(60)
        sign_in = browser.find_element_by_name('emailOrPhone')
        sign_in.clear()
        sign_in.send_keys('admin@example.com')
        browser.implicitly_wait(60)
        password = browser.find_element_by_name('password')
        password.clear()
        password.send_keys('password')
        browser.implicitly_wait(60)
        password.submit()
     

    def test_checkin(self):
        self.test_login()
        browser = self.browser
        browser.implicitly_wait(60)
        try:
            patient_name = '  '.join([line.rstrip() for line in open('test_patient.txt')][1:3])
        except Exception as e:
            print(e)
        print(patient_name)
        patient = browser.find_element_by_xpath('//span[text()="%s"]' % patient_name)
        patient_parent = patient.find_element_by_xpath('../..')
        checkin_btn = patient_parent.find_element_by_class_name('Patients__checkin')
        checkin_btn.click()
        browser.implicitly_wait(60)
        modal = browser.find_element_by_class_name('modal-open')
        visit_type_field = browser.find_element_by_xpath('//*[@id="consultation"]/div[3]/div[1]/billing-items-select-with-categories/div/div/span')
        visit_type_field.click()
        browser.implicitly_wait(60)
        visit_type = browser.find_element_by_xpath('//*[@id="consultation"]/div[3]/div[1]/billing-items-select-with-categories/div/ul/li[3]/span[1]/span')
        visit_type.click()
        to_see = browser.find_element_by_xpath('//*[@id="consultation"]/div[4]/div/div[1]/radio-buttons/div[2]/label/span')
        # Selects team
        to_see.click()
        # Choose team - Doctors
        team = browser.find_element_by_xpath('//*[@id="consultation"]/div[4]/div/div[2]/div[1]/span')
        team.click()
        doctors_team = browser.find_element_by_xpath('//span[text()="Doctors"]')
        checkin = browser.find_element_by_xpath('//*[@id="consultation"]/div[5]/input')
        checkin.submit()
        browser.implicitly_wait(60)
        # Selects Doctors
        checkbox = browser.find_element_by_xpath('/html/body/div[1]/div/div/forward-file-modal/div/div/div[3]/div[1]/div[1]/div[1]/div[1]')
        checkbox.click()
        forward = browser.find_element_by_xpath('/html/body/div[1]/div/div/forward-file-modal/div/div/div[3]/div[2]/div[2]/div')
        forward.click()
        
 


    
    def tearDown(self):
        self.browser.quit()
